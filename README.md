# HOWTO

### Run container:
    docker run -d -p 4443:443 --name file_server \
      -v /dir/with/files_to/serve:/usr/share/nginx/html:ro -v /etc/localtime:/etc/localtime:ro \
      -e USERNAME=username -e PASSWORD=password -e COMMON_NAME=put_valid_hostname_or_localhost floulab/file_server
