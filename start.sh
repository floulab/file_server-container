#!/bin/bash
set -e

if [ ! -f /etc/nginx/server.crt ] || [ ! -f /etc/nginx/server.key ]; then
  openssl req -new -newkey rsa:4096 -days 3650 -nodes -x509 \
    -subj "/C=$COUNTRY/ST=$STATE/L=$CITY/O=$ORGANIZATION/CN=$COMMON_NAME" \
    -keyout /etc/nginx/server.key  -out /etc/nginx/server.crt
  chown nginx /etc/nginx/server.key && chmod 600 /etc/nginx/server.key
fi

if [ ! -f /etc/nginx/htpasswd ]; then
  echo -n "$USERNAME:" >> /etc/nginx/htpasswd && openssl passwd -apr1 $PASSWORD >> /etc/nginx/htpasswd
  chown nginx /etc/nginx/htpasswd && chmod 600 /etc/nginx/htpasswd
fi

exec nginx -g 'daemon off;'
